package tests;

import helpers.ConfigFileReader;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;


public class BeforeAfterTests {
    protected WebDriver driver;
    protected ConfigFileReader reader;

    @Attachment(value = "Page screenshot", type="image/png")
    public byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    @BeforeMethod
    public void beforeAllTests() {
        reader = new ConfigFileReader();
        System.setProperty("webdriver.chrome.driver", reader.getChromeDriverPath());
        driver = new ChromeDriver();
        driver.get(reader.getApplicationUrl());
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void afterEachTest() {
        saveScreenshotPNG(driver);
        driver.close();
    }

}
