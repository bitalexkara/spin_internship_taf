package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class MainPage extends BasePage {
    private By userRole = By.xpath("//div/span[@class='user-role']");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    @Step("Get user role step")
    public String getUserRole(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));  //Wait up to 5 secs until element becomes visible
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(userRole));
        return element.getText();
    }
}