package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

public class SignInPage extends BasePage {
    private By emailTextField = By.xpath("//input[@type='email']");

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    @Step("Type email with email: {0}, for method: {method} step")
    public void typeLogin(String user) {
        driver.findElement(emailTextField).sendKeys(user);
    }
}
